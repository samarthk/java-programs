
public class PolynomialTester {
	public static void main(String[] args) {
		Term t1 = new Term(0, 0);
		Term t2 = new Term(1, 1);
		Term t3 = new Term(2, 0);
		Term t4 = new Term(2.6, 2);
		
		Polynomial a = new Polynomial(t1);
		System.out.println(a.toFormattedString());
		Polynomial b = new Polynomial(t2);
		Polynomial c = new Polynomial(t3);
		System.out.println(c.toFormattedString());
		Polynomial d = new Polynomial(t4);
		
		Polynomial e = a.add(b);
		Polynomial f = e.add(c);
		Polynomial g = f.add(d);
		
		System.out.println(g.toFormattedString());
		
		System.out.println(g.eval(2));
		System.out.println(g.eval(-2));
	}
}
