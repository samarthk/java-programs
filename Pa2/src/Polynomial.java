// Name:Samarth Kulkarni
// USC loginid: 3380461872
// CS 455 PA2
// Spring 2017


import java.util.ArrayList;

/**
   A polynomial. Polynomials can be added together, evaluated, and
   converted to a string form for printing.
*/
public class Polynomial {

	private ArrayList<Term> terms = null;
	

    /**
       Creates the 0 polynomial
    */
    public Polynomial() {
    	this.terms = new ArrayList<Term>();
    }


    /**
     	Creates polynomial with single term given
     	PRE: coeff > 0 and expon >= 0;
       
     */
    public Polynomial(Term term) {
    	this();
    	if(term.getExpon() >= 0){
    		this.terms.add(term);
    	}
    	assert this.isValidPolynomial();

    }
    
    /**
       Returns the Polynomial that is the sum of this polynomial and b
       (neither poly is modified)
     */
    public Polynomial add(Polynomial b) {
    	Polynomial resultPoly = new Polynomial();
    	
    	int indexA = 0;
    	int indexB = 0;
    	
    	assert this.isValidPolynomial();
    	assert b.isValidPolynomial();
    	
    	
    	while (indexA < this.terms.size() && indexB < b.terms.size()){ // Sorts two polynomial array based on merge sort. if both elements from array are equal we add them. 
    		Term termA = this.terms.get(indexA);
    		Term termB = b.terms.get(indexB);
    		
    		if(termA.getExpon() == termB.getExpon()){
    			double sum = termA.getCoeff()+termB.getCoeff();
    			if(sum != 0){
    				resultPoly.terms.add(new Term (sum, termA.getExpon()));
    			}
    			indexA++;
    			indexB++;
    		}
    		else if(termA.getExpon() > termB.getExpon()){
    			resultPoly.terms.add(termA);
    			indexA++;
    		}
    		else{
    			resultPoly.terms.add(termB);
    			indexB++;
    		}
    	}
    	
    	while (indexA < this.terms.size()){
    		resultPoly.terms.add(this.terms.get(indexA));
    		indexA++;
    	}
    	while (indexB < b.terms.size()){
    		resultPoly.terms.add(b.terms.get(indexB));
    		indexB++;
    	}
    	assert resultPoly.isValidPolynomial();
    	return resultPoly;
    }


    /**
       Returns the value of the poly at a given value of x.
     */
    public double eval(double x) {
    	double result = 0.0;
    
    	for(Term term : terms){
    		result += (term.getCoeff() * Math.pow(x, term.getExpon()));
    	}
    	
    	return result;
    }


    /**
       Return a String version of the polynomial with the 
       following format, shown by example:
       zero poly:   "0.0"
       1-term poly: "3.2x^2"
       4-term poly: "3.0x^5 + -x^2 + x + -7.9"

       Polynomial is in a simplified form (only one term for any exponent),
       with no zero-coefficient terms, and terms are shown in
       decreasing order by exponent.
    */
    public String toFormattedString() {
    	String output = "";
    	if(this.terms.size() < 1){
    		output = "0.0";
    	}
    	for(int i = 0; i < this.terms.size(); i++){
    		if(terms.get(i).getCoeff() != 0.0 || terms.get(i).getExpon() != 0){
    			if(terms.get(i).getExpon() == 0 && terms.get(i).getCoeff() != 0.0){
        			output += terms.get(i).getCoeff();
        		}
    			else if(terms.get(i).getCoeff() == 0);
    			else if (terms.get(i).getExpon() == 1){
    				if(terms.get(i).getCoeff() == 1.0){
        				output += "x";
        			}
        			else if(terms.get(i).getCoeff() == -1.0){
        				output += "-x";
        			}
        			else{
        				output += terms.get(i).getCoeff()+"x";
        			}
    			}
    			else if(terms.get(i).getCoeff() == 1.0){
    				output += "x^"+terms.get(i).getExpon();
    			}
    			else if(terms.get(i).getCoeff() == -1.0){
    				output += "-x^"+terms.get(i).getExpon();
    			}
    			else{
    				output += terms.get(i).getCoeff()+"x^"+terms.get(i).getExpon();
        		}
    			if(i != this.terms.size()-1 && terms.get(i).getCoeff() != 0 ){
        			output += " + ";
        		}
    		}
    		else{
    			if(this.terms.size() == 1){
    				output = "0.0";
    			}
    		}
    	}
    	return output;
    }


    // **************************************************************
    //  PRIVATE METHOD(S)

    /**
       Polynomial is valid when each term is in sorted order.
       and exponent of each term has to be positive.
       Returns true iff the poly data is in a valid state.
    */
    private boolean isValidPolynomial() {	
    	boolean isValid = true;
    	for(int i = 0; i < terms.size()-1; i++){
    		if(this.terms.get(i).getExpon() < 0 || this.terms.get(i).getExpon() < this.terms.get(i+1).getExpon()){
    			isValid = false;
    		}
    	}
	return isValid;
    }

}
