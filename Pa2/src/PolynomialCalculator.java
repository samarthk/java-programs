// Name:Samarth Kulkarni
// USC loginid: 3380461872
// CS 455 PA2
// Spring 2017

import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * Console interface to do polynomial operations on list 10 polynomial elements.
 * create, print, evaluate, add are the operation.
 * we have call exit to go go out of the interface.
 * @author Samarth Kulkarni
 *
 */
public class PolynomialCalculator {
	
	private static final String HELP = "help";
	private static final String CREATE = "create";
	private static final String	PRINT = "print";
	private static final String ADD = "add";
	private static final String EVAL = "eval";
	private static final String QUIT = "quit";
	
	private static Polynomial[] polynomials = new Polynomial[10];
	
	public static void main(String[] args) {
		boolean stayOnInterface = true;
		
		initaializePolyArr();
		
		while(stayOnInterface){
			System.out.print("cmd> ");
			Scanner in = new Scanner(System.in);
			String operation = in.next();
			
			if(operation.equalsIgnoreCase(CREATE)){
				createPolynomial(in);
			}
			else if(operation.equalsIgnoreCase(PRINT)){
				printPolynomial(in);
			}
			else if(operation.equalsIgnoreCase(ADD)){
				addPolynomials(in);
			}
			else if(operation.equalsIgnoreCase(EVAL)){
				evalutatePolynomial(in);
			}
			else if(operation.equalsIgnoreCase(QUIT)){
				stayOnInterface = false;
			}
			else if(operation.equalsIgnoreCase(HELP)){
				printAvailCmds();
			}
			else {
				cmdErrorMsg();
				
			}
		}
	}

	/**
	 * Initializes polynomial array.
	 * Size of array is hard-coded to 10.
	 */
	private static void initaializePolyArr() {
		for(int i = 0; i < polynomials.length; i++){
			polynomials[i] = new Polynomial();
		}
		
	}

	/**
	 * Prints the list of available commands.
	 */
	private static void printAvailCmds() {
		System.out.println("Here are the list of valid commands,");
		System.out.println("create <index> - It is used to create polynomial at specified index");
		System.out.println("add <result index> <first poly. index> <second poly. index - addes polynomial with 'first poly. index'"
				+ "and 'second poly. index'. Stores result polynomial in 'result index'");
		System.out.println("eval <index> - for polynomial of specified index it evaluates expression with the double value given");
		System.out.println("print <index> - prints the polynomial of specified index");
		System.out.println();
		System.out.println("Note -  Indices of polynomials must be between 0 and 9, inclusive");
		
	}

	/**
	 * It evaluates the polynomial expression of given index for a specified floating point value.
	 * PRE: index have to be in range of 0 to 9 inclusive.
	 */
	private static void evalutatePolynomial(Scanner in) {
		if(in.hasNextInt()){
			int index = in.nextInt();
			assert isValidIndex(index);
			if (isValidIndex(index)) {
				Scanner valScan = new Scanner(System.in);
				System.out.print("Enter a floating point value for x: ");
				double val = valScan.nextDouble();
				double result = polynomials[index].eval(val);
				System.out.println(result);
			}
			else{
				indexErrMsg();
			}
		}
		
	}

	/**
	 * Adds two polynomials and stores the result.
	 * PRE: indices have to be in range of 0 to 9 inclusive.
	 */
	private static void addPolynomials(Scanner in) {
		if(in.hasNextInt()){
			int resultPolyIndex = in.nextInt();
			int firstPolyIndex = in.nextInt();
			int secondPolyIndex = in.nextInt();	
			assert isValidIndex(resultPolyIndex);
			assert isValidIndex(firstPolyIndex);
			assert isValidIndex(secondPolyIndex);
			if(isValidIndex(resultPolyIndex) && isValidIndex(firstPolyIndex) && isValidIndex(secondPolyIndex)){
				polynomials[resultPolyIndex] = polynomials[firstPolyIndex].add(polynomials[secondPolyIndex]);
			}
			else{
				indexErrMsg();
			}
		}
		
	}

	/**
	 * Prints the polynomial with specified index
	 * PRE: indices have to be in range of 0 to 9 inclusive.
	 */
	private static void printPolynomial(Scanner in) {
		if(in.hasNextInt()){
			int index = in.nextInt();
			assert isValidIndex(index);
			if (isValidIndex(index)) {
				System.out.println(polynomials[index].toFormattedString());
			}
			else{
				indexErrMsg();
			}
		}
		else{
			cmdErrorMsg();
		}
		
	}

	/**
	 * Creates new polynomial at the given index.
	 * PRE:  index have to be in range of 0 to 9 inclusive.
	 * If exponent is missing for the last term (meaning entered space separated values have odd numbers) it discards coefficient as well.
	 * If the value of exponent is negative the it converts to absolute value.
	 * 
	 */
	private static void createPolynomial(Scanner in) {
		if(in.hasNextInt()){
			int index = in.nextInt();
			if(isValidIndex(index)){
				polynomials[index] = new Polynomial(); // creates new polynomial at that index.
				System.out.println("Enter a space-separated sequence of coeff-power pairs terminated by <nl>");
				Scanner pNums = new Scanner(System.in);
				String terms = pNums.nextLine();
				String[] strTermsArr = terms.trim().split("\\s+");
				int termsArrLen = strTermsArr.length;
				if(strTermsArr.length > 0){ 
					if(strTermsArr.length % 2 == 0){ // To check whether all terms have both coefficient and exponent else discard last coefficient.
						termsArrLen = strTermsArr.length;
					}
					else{
						termsArrLen = strTermsArr.length - 1;
						System.out.println("WARNING: Last term ignored as there was no corresponding exponent given for the cofficient");
					}
				
					for(int i = 0; i < termsArrLen; i=i+2){ // adding terms to polynomial and converting negative coefficients to absolute value.
						int expon = Integer.parseInt(strTermsArr[i+1]);
						double coeff = Double.parseDouble(strTermsArr[i]);
						if( expon >= 0 ){
							polynomials[index] = polynomials[index].add(new Polynomial(new Term(coeff, expon)));
						}
						else{
							polynomials[index] = polynomials[index].add(new Polynomial(new Term(coeff, (expon*-1))));
							System.out.println("WARNING: One or more terms contain negative exponents, we have taken the absolute value of it");
						}
					}
				}
			}
			else{
				indexErrMsg();
			}
		}
	}
	
	/**
	 * Check whether given index is within specified range (0 to 9 inclusive).
	 * returns true if index is within range and false if it is cout of range.
	 */
	private static boolean isValidIndex(int index) {
		return (index >= 0 && index < 10);
	}
	
	/**
	 * Error message when index is out of specified range.
	 */
	private static void indexErrMsg() {
		System.out.println("ERROR: illegal index for a poly.  must be between 0 and 9, inclusive");
	}
	
	/**
	 * Error messgae if command is wrong.
	 */
	private static void cmdErrorMsg() {
		System.out.println("ERROR: Illegal command. Type 'help' for command options");
	}
}
